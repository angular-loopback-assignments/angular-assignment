import { Injectable } from '@angular/core';
import User from './shared/users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  userList: Array<User> = [
    {
      userId: 1,
      firstName: "John",
      middleName: "",
      lastName: "Doe",
      email: "john1@gmail.com",
      phoneNo: 1234567890,
      role: "Super Admin",
      address: "Los Angeles"
    },
    {
      userId: 2,
      firstName: "John",
      middleName: "Mark",
      lastName: "Doe",
      email: "john2@gmail.com",
      phoneNo: 1234567891,
      role: "Admin",
      address: "California"
    },
    {
      userId: 3,
      firstName: "John",
      middleName: "Mark",
      lastName: "Doe",
      email: "john3@gmail.com",
      phoneNo: 1234567892,
      role: "User",
      address: "Texas"
    }
  ]
}
