export default interface User {
    userId:number;
    firstName: string;
    middleName: string;
    lastName: string;
    email: string;
    phoneNo: number;
    role: string;
    address: string;
    enableEdit?: boolean;
}
