import { Component, OnInit } from '@angular/core';
import User from '../shared/users';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  constructor(private userService: UsersService) { }

  refreshStatus = false;
  tableStatus = false;
  // buttonTitle = 'Refresh Users';
  buttonTitle = 'Load Users';
  usersArray: Array<User> = [];
  usersToDisplay: Array<User> = [];

  ngOnInit(): void {
  }

  handleUserAction() {
    if (this.buttonTitle == 'Load Users') {
      this.buttonTitle = 'Refresh Users';
      this.tableStatus = true;
      this.usersArray = this.userService.userList;
      this.usersToDisplay = this.userService.userList;
    }
    else if (this.buttonTitle == 'Refresh Users') {
      this.usersToDisplay = this.usersArray;
    }
  }

  deleteUser(user: User) {
    this.usersToDisplay = this.usersToDisplay.filter(selectedUser => selectedUser.userId != user.userId)
  }

  updateUser(user: User) {
    
    let newUserArray: User[] = this.usersToDisplay.map(myUser => {
      if (myUser.userId === user.userId) {
        myUser = user
        myUser.enableEdit = false
      }
      return myUser;
    })
    this.usersToDisplay = newUserArray;
  }

}
